/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jadva3;

/**
 *
 * @author Ho Hang
 */
public class savingsAccount {
    
    public static double annualInterestRate = 0.04;
    
    public double calculateMonthlyInterest (double savingsBalance){
        return annualInterestRate*savingsBalance/12;
    }
    
}
