/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jadva3;

/**
 *
 * @author Ho Hang
 */
public class Jadva3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //create new accounts
        savingsAccount saver1 = new savingsAccount();
        double interest1 = saver1.calculateMonthlyInterest(2000);
        
        //calculate interest 
        double newBalance1 = 2000+interest1;
        
        //display interest
        System.out.println(String.format("New balance for saver 1: $%s", Math.round(newBalance1*100.0)/100.0));
        
        //repeat above for second account and new interest rate
        savingsAccount saver2 = new savingsAccount();
        double interest2 = saver2.calculateMonthlyInterest(5000);        
        double newBalance2 = 5000+interest2;
        
        
        System.out.println(String.format("New balance for saver 2: $%s", Math.round(newBalance2*100.0)/100.0));
        saver1.annualInterestRate = 0.05;
        interest1 = saver1.calculateMonthlyInterest(2000);
        newBalance1 = 2000+interest1;
        System.out.println(String.format("New balance for saver 1: $%s", Math.round(newBalance1*100.0)/100.0));        
        saver2.annualInterestRate = 0.05;
        interest2 = saver2.calculateMonthlyInterest(5000);        
        newBalance2 = 5000+interest2;
        System.out.println(String.format("New balance for saver 2: $%s", Math.round(newBalance2*100.0)/100.0));
        
    }
    
}
